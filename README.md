# Postfix
---
Made because I needed a well configurable installation of postfix.  
I also wanted to ensure proper use of tls.

Only **SMTP** and **SUBMISSION** are enabled by default.

This image uses **SASL** and **LMTP**, I use it in combination with my dovecot image.

## Configuration by environment variables (as seen in Dockerfile):

    MAIL_DOMAIN
    MAIL_ALIAS_DOMAINS
    POSTFIX_HOSTNAME
    POSTFIX_SSL_CERT_PATH
    POSTFIX_SSL_CERT_KEY
    SASL_HOST
    SASL_PORT
    LMTP_HOST
    LMTP_PORT
    SPAMASSASSIN (default set to false)

## Mapping
For Mapping these locations are used:

    /var/mail/virtual_aliases 
    /var/mail/virtual_mailbox_domains 

