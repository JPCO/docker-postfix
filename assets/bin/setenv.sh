#!/bin/sh
# Should be only one
echo $MAIL_DOMAIN > /etc/mailname

postconf -e   smtpd_tls_cert_file=${POSTFIX_SSL_CERT_PATH} \
  smtpd_tls_key_file=${POSTFIX_SSL_CERT_KEY} \
  smtpd_sasl_path=inet:${SASL_HOST}:${SASL_PORT} \
  virtual_transport=lmtp:inet:${LMTP_HOST}:${LMTP_PORT} \
  myhostname=${POSTFIX_HOSTNAME} \
  mydomain=${MAIL_DOMAIN}
  
if [ ! -f /var/mail/virtual_aliases ]; then
  touch /var/mail/virtual_aliases
fi
if [ ! -f /var/mail/virtual_mailbox_domains ]; then
  echo "${MAIL_ALIAS_DOMAINS} OK" > /var/mail/virtual_mailbox_domains
fi
if [ "${SPAMASSASSIN}" = "true" ]; then
   cp /etc/postfix/master.spamassassin.cf /etc/postfix/master.cf
else
   cp /etc/postfix/master.regular.cf /etc/postfix/master.cf	
fi
postmap /var/mail/virtual_aliases
postmap /var/mail/virtual_mailbox_domains
