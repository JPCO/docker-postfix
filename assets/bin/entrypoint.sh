#!/bin/sh
setenv.sh
postfix -v check
service rsyslog start
service postfix start
if [ "${SPAMASSASSIN}" = "true" ]; then
   service spamassassin start
fi
exec "$@"
